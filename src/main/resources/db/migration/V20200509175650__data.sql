INSERT INTO account(id, name)
VALUES ('44d307f0-18db-442a-a5ec-d722bf09f9a5', 'NetRadius');

INSERT INTO namespace(id, account, name)
VALUES ('be8c79f6-7fa1-4b4a-bc49-849f825229ec', '44d307f0-18db-442a-a5ec-d722bf09f9a5', 'nr');

INSERT INTO location(id, account, name)
VALUES ('b43b074c-a980-46aa-91c4-68b2b7eed11d', '44d307f0-18db-442a-a5ec-d722bf09f9a5', 'aws-us-west-2');

-- Create an IAM service in aws-us-west-2 with a DirectAuth action
INSERT INTO service(id, account, name, global)
VALUES ('78b36954-66e0-4c2a-a5e2-7501e6fd9be3', '44d307f0-18db-442a-a5ec-d722bf09f9a5', 'iam', true);
INSERT INTO service_availability(service, location)
VALUES ('78b36954-66e0-4c2a-a5e2-7501e6fd9be3', 'b43b074c-a980-46aa-91c4-68b2b7eed11d');
INSERT INTO service_action(id, service, name)
VALUES ('8148d044-6273-4e0f-afa5-4bb24936f4c6', '78b36954-66e0-4c2a-a5e2-7501e6fd9be3', 'DirectAuth');

-- Create a Hello service in aws-us-west-2 which a Hello action
INSERT INTO service(id, account, name, global)
VALUES ('fb2a8194-5e55-474f-acb0-f8afc7f19d83', '44d307f0-18db-442a-a5ec-d722bf09f9a5', 'hello', true);
INSERT INTO service_availability(service, location)
VALUES ('fb2a8194-5e55-474f-acb0-f8afc7f19d83', 'b43b074c-a980-46aa-91c4-68b2b7eed11d');
INSERT INTO service_action(id, service, name)
VALUES ('06311092-bb4c-4044-a3bb-6966baf678cc', 'fb2a8194-5e55-474f-acb0-f8afc7f19d83', 'Hello');

-- Create a HelloServer application that supplies the Hello action
INSERT INTO application(id, account, name)
VALUES ('b51e4e2e-5f19-4761-95f4-8e503a97b143', '44d307f0-18db-442a-a5ec-d722bf09f9a5', 'HelloServer');
INSERT INTO application_supplied_action(application, action)
VALUES ('b51e4e2e-5f19-4761-95f4-8e503a97b143', '06311092-bb4c-4044-a3bb-6966baf678cc');
INSERT INTO application_v1_hmac_sha512_key(id, application, secret)
VALUES ('5cb5acc7-6fb7-409c-be17-319fb7cb9568', 'b51e4e2e-5f19-4761-95f4-8e503a97b143', (select decode('S0pdHNsMnxCzlQJkVTUODFqSC+JPvJuMImAPz4E1rZPZt5aLDe3rZIEs2WZk3CY+111WBX+Ej990BpMi1asuvg==', 'base64')));

-- Create a HelloServer policy that allows it to use DirectAuth
INSERT INTO policy(id, account, name, global, statements)
VALUES ('6556d75a-2b0c-40fa-8b35-f2deaecf077c', '44d307f0-18db-442a-a5ec-d722bf09f9a5', 'HelloServer', false, '[{"deny": [],"allow": ["nr/iam/actions/DirectAuth"],"resources": []}]' );
INSERT INTO policy_rule(id, policy, allow, service, action, resource_type, resource_id)
VALUES ('c0129e1d-0552-41c6-9755-65e49743fe72', '6556d75a-2b0c-40fa-8b35-f2deaecf077c', true, '78b36954-66e0-4c2a-a5e2-7501e6fd9be3', '8148d044-6273-4e0f-afa5-4bb24936f4c6', null, null);
INSERT INTO policy_assignment(policy, application)
VALUES ('6556d75a-2b0c-40fa-8b35-f2deaecf077c', 'b51e4e2e-5f19-4761-95f4-8e503a97b143');

-- Create a HelloClient Application with a secret key
INSERT INTO application(id, account, name)
VALUES ('2fb06e04-47f3-4296-9595-1c33ff5e5790', '44d307f0-18db-442a-a5ec-d722bf09f9a5', 'HelloClient');
INSERT INTO application_v1_hmac_sha512_key(id, application, secret)
VALUES ('c15c50fb-e089-4cb9-a418-5c0221dfced7', '2fb06e04-47f3-4296-9595-1c33ff5e5790', (select decode('frUkGTiG6xm/7dGb5wmStuMIbZiYgPvVdYZTu2E8SkCAdq91ou2RMItA+L/iL2XilVttd7yzC9yQZSWzM4Ancg==', 'base64')));

-- Create a HelloClient Policy that allows HelloClient to call Hello on Hello Service
INSERT INTO policy(id, account, name, global, statements)
VALUES ('e8c004a7-f7f6-4d3e-8f89-73b62c034064', '44d307f0-18db-442a-a5ec-d722bf09f9a5', 'HelloClient', false, '[{"deny": [],"allow": ["nr/hello/actions/Hello"],"resources": []}]');
INSERT INTO policy_rule(id, policy, allow, service, action, resource_type, resource_id)
VALUES ('b7e508e7-b8eb-49c6-8aa8-df0e8c363cd8', 'e8c004a7-f7f6-4d3e-8f89-73b62c034064', true, 'fb2a8194-5e55-474f-acb0-f8afc7f19d83', '06311092-bb4c-4044-a3bb-6966baf678cc', null, null);
INSERT INTO policy_assignment(policy, application)
VALUES ('e8c004a7-f7f6-4d3e-8f89-73b62c034064', '2fb06e04-47f3-4296-9595-1c33ff5e5790');

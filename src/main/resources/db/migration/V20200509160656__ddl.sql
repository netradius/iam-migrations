CREATE TABLE account
(
    id UUID NOT NULL,
    name VARCHAR(100) NOT NULL,
    CONSTRAINT pk_account PRIMARY KEY (id)
);

CREATE UNIQUE INDEX uidx_account_name ON account(lower(name));

CREATE TABLE namespace
(
    id UUID NOT NULL,
    account UUID NOT NULL,
    name VARCHAR(10) NOT NULL,
    CONSTRAINT pk_namespace PRIMARY KEY (id),
    CONSTRAINT fk_namespace_account FOREIGN KEY (account) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX idx1_namespace ON namespace(account, lower(name));
CREATE UNIQUE INDEX uidx_namespace_name ON namespace(lower(name));

CREATE TABLE location
(
    id UUID NOT NULL,
    account UUID NOT NULL,
    name VARCHAR(30) NOT NULL,
    CONSTRAINT pk_location PRIMARY KEY (id),
    CONSTRAINT fk_location_account FOREIGN KEY (account) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX idx1_location ON location(account);
CREATE UNIQUE INDEX uidx_location_name ON location(account, lower(name));

CREATE TABLE service
(
    id UUID NOT NULL,
    account UUID NOT NULL,
    name VARCHAR(10) NOT NULL,
    global BOOLEAN NOT NULL DEFAULT false,
    CONSTRAINT pk_service PRIMARY KEY (id),
    CONSTRAINT fk_service_account FOREIGN KEY (account) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX idx1_service ON service(account, lower(name));
CREATE UNIQUE INDEX uidx_service_name ON service(account, lower(name));

CREATE TABLE service_share
(
    service UUID NOT NULL,
    account UUID NOT NULL,
    CONSTRAINT pk_service_share PRIMARY KEY (service, account),
    CONSTRAINT fk_service_share_service FOREIGN KEY (service) REFERENCES service(id) ON DELETE CASCADE,
    CONSTRAINT fk_service_share_account FOREIGN KEY (account) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX idx1_service_share ON service_share(account);

CREATE TABLE service_availability
(
    service UUID NOT NULL,
    location UUID NOT NULL,
    CONSTRAINT pk_service_availability PRIMARY KEY (service, location),
    CONSTRAINT fk_service_availability_service FOREIGN KEY (service) REFERENCES service(id) ON DELETE CASCADE,
    CONSTRAINT fk_service_availability_location FOREIGN KEY (location) REFERENCES location(id) ON DELETE CASCADE
);

CREATE INDEX idx1_service_availability ON service_availability(location);

CREATE TABLE service_resource_type
(
    id UUID NOT NULL,
    service UUID NOT NULL,
    name VARCHAR(30) NOT NULL,
    CONSTRAINT pk_service_resource_type PRIMARY KEY (id),
    CONSTRAINT fk_service_resource_type_service FOREIGN KEY (service) REFERENCES service(id) ON DELETE CASCADE
);

CREATE INDEX idx1_service_resource_type ON service_resource_type(service);
CREATE UNIQUE INDEX uidx_service_resource_type_name ON service_resource_type(service, lower(name));

CREATE TABLE service_action
(
    id UUID NOT NULL,
    service UUID NOT NULL,
    name VARCHAR(30) NOT NULL,
    CONSTRAINT pk_action PRIMARY KEY (id),
    CONSTRAINT fk_action_service FOREIGN KEY (service) REFERENCES service(id) ON DELETE CASCADE
);

CREATE INDEX idx1_service_action ON service_action(service, lower(name));
CREATE UNIQUE INDEX uidx_service_action_name ON service_action(service, lower(name));

CREATE TABLE application
(
    id UUID NOT NULL,
    account UUID NOT NULL,
    name VARCHAR(30) NOT NULL,
    CONSTRAINT pk_application PRIMARY KEY (id),
    CONSTRAINT fk_application_account FOREIGN KEY (account) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX idx1_application ON application(account);
CREATE UNIQUE INDEX uidx_application_name ON application(account, lower(name));

CREATE TABLE application_share (
    application UUID NOT NULL,
    account UUID NOT NULL,
    CONSTRAINT pk_application_share PRIMARY KEY (application, account),
    CONSTRAINT fk_application_share_application FOREIGN KEY (application) REFERENCES application(id) ON DELETE CASCADE,
    CONSTRAINT fk_application_share_account FOREIGN KEY (account) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX idx1_application_share ON application_share(account);

CREATE TABLE application_supplied_action (
    application UUID NOT NULL,
    action UUID NOT NULL,
    CONSTRAINT pk_application_supplied_action PRIMARY KEY (application, action),
    CONSTRAINT fk_application_supplied_action_application FOREIGN KEY (application) REFERENCES application(id) ON DELETE CASCADE,
    CONSTRAINT fk_application_supplied_action_action FOREIGN KEY (action) REFERENCES service_action(id) ON DELETE CASCADE
);

CREATE INDEX idx1_application_supplied_action ON application_supplied_action(action);

CREATE TABLE application_v1_hmac_sha1_key
(
    id UUID NOT NULL,
    application UUID NOT NULL,
    secret BYTEA NOT NULL,
    CONSTRAINT pk_application_v1_hmac_sha1_key PRIMARY KEY (id),
    CONSTRAINT fk_application_v1_hmac_sha1_key_application FOREIGN KEY (application) REFERENCES application(id) ON DELETE CASCADE
);

CREATE INDEX idx1_application_v1_hmac_sha1_key ON application_v1_hmac_sha1_key(application);

CREATE TABLE application_v1_hmac_sha512_key
(
    id UUID NOT NULL,
    application UUID NOT NULL,
    secret BYTEA NOT NULL,
    CONSTRAINT pk_application_v1_hmac_sha512_key PRIMARY KEY (id),
    CONSTRAINT fk_application_v1_hmac_sha512_key_application FOREIGN KEY (application) REFERENCES application(id) ON DELETE CASCADE
);

CREATE INDEX idx1_application_v1_hmac_sha512_key ON application_v1_hmac_sha512_key(application);

CREATE TABLE policy
(
    id UUID NOT NULL,
    account UUID NOT NULL,
    name VARCHAR(30) NOT NULL,
    global BOOLEAN NOT NULL DEFAULT false,
    statements JSON NOT NULL,
    CONSTRAINT pk_policy PRIMARY KEY (id),
    CONSTRAINT fk_policy_account FOREIGN KEY (account) REFERENCES account(id) ON DELETE CASCADE
);

CREATE INDEX idx1_policy ON policy(account);
CREATE UNIQUE INDEX uidx_policy_name ON policy(account, lower(name));

CREATE TABLE policy_assignment
(
    policy UUID NOT NULL,
    application UUID NOT NULL,
    CONSTRAINT pk_policy_assignment PRIMARY KEY (policy, application),
    CONSTRAINT fk_policy_assignment_policy FOREIGN KEY (policy) REFERENCES policy(id) ON DELETE CASCADE,
    CONSTRAINT fk_policy_assignment_application FOREIGN KEY (application) REFERENCES application(id) ON DELETE CASCADE
);

CREATE INDEX idx1_policy_assignment ON policy_assignment(application);

CREATE TABLE policy_rule
(
    id UUID NOT NULL,
    policy UUID NOT NULL,
    allow BOOLEAN NOT NULL,
    service UUID NOT NULL,
    action UUID,
    resource_type UUID,
    resource_id UUID,
    CONSTRAINT pk_policy_rule PRIMARY KEY (id),
    CONSTRAINT fk_policy_rule_policy FOREIGN KEY (policy) REFERENCES policy(id) ON DELETE CASCADE,
    CONSTRAINT fk_policy_rule_service FOREIGN KEY (service) REFERENCES service(id) ON DELETE CASCADE,
    CONSTRAINT fk_policy_rule_action FOREIGN KEY (action) REFERENCES service_action(id) ON DELETE CASCADE,
    CONSTRAINT fk_policy_rule_resource_type FOREIGN KEY (resource_type) REFERENCES service_resource_type ON DELETE CASCADE
);

CREATE INDEX idx1_policy_rule ON policy_rule(policy, service, action, resource_type, resource_id);

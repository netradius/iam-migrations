#!/usr/bin/env bash


DIR=$(dirname "${0}")
cd "${DIR}" || exit 1
CWD=$(pwd)

STACK_PROJ="iam-localdev"
STACK_FILE="localdev-docker-compose.yml"
VOLUMES="${STACK_PROJ}_pgdata"
DB_CONTAINER="${STACK_PROJ}_db_1"
DB_NAME="$(grep POSTGRES_DB ${STACK_FILE} | sed 's/.*://g' | tr -d '[:space:]')"
DB_USER="$(grep POSTGRES_USER ${STACK_FILE} | sed 's/.*://g' | tr -d '[:space:]')"
DB_PASSWORD="$(grep POSTGRES_PASSWORD ${STACK_FILE} | sed 's/.*://g' | tr -d '[:space:]')"
DB_PORT="$(grep :5432 ${STACK_FILE}| sed -e 's/:.*//g' -e 's/-//g' | tr -d '[:space:]')"

function usage() {
  echo "Usage $0 [COMMAND]"
  printf "  %-10s %s\n" "start" "Starts the local development stack"
  printf "  %-10s %s\n" "stop" "Stops the local development stack"
  printf "  %-10s %s\n" "destroy" "Destroys the local development stack"
  printf "  %-10s %s\n" "psql" "Starts a psql prompt to the local DB"
}

function stop() {
  echo "Stopping ${STACK_PROJ}"
  docker-compose -p ${STACK_PROJ} -f ${STACK_FILE} stop
}

function start() {
  echo "Starting ${STACK_PROJ}"
  docker-compose -p ${STACK_PROJ} -f ${STACK_FILE} up --build --abort-on-container-exit
}

function destroy() {
  echo "Destroying ${STACK_PROJ}"
  docker-compose -p ${STACK_PROJ} -f ${STACK_FILE} rm -f
  for v in ${VOLUMES}; do
    echo "Destroying volume ${v}"
    set -x
    docker volume rm ${v}
  done
}

function psql() {
  docker exec -it ${DB_CONTAINER} su - postgres -c "psql -U ${DB_USER} ${DB_NAME}"
}

function migrate() {
  cd .. || exit 1
  ./mvnw -Dflyway.user="${DB_USER}" -Dflyway.password="${DB_PASSWORD}" -Dflyway.url="jdbc:postgresql://127.0.0.1:${DB_PORT}/${DB_NAME}" flyway:migrate
  cd "${CWD}" || exit 1
}

COMMAND="${1}"

if [ -z "${COMMAND}" ]; then
  echo "Command is required"
  usage
  exit 1
fi

case ${COMMAND} in
  start)
    stop; start ;;
  stop)
    stop ;;
  destroy)
    stop; destroy ;;
  psql)
    psql ;;
  migrate)
    migrate ;;
  *)
    echo "Unknown command: ${COMMAND}"
    usage
    exit 1
    ;;
esac

#!/usr/bin/env bash

DIR=$(dirname "${0}")
cd "${DIR}/.." || exit 1

function usage() {
  echo "Usage $0 [COMMAND]"
  printf "  %-10s %s\n" "new" "Creates new migration script"
}

function new() {
  DATE=$(date "+%Y%m%d%H%M%S")
  echo "What do you want to name this script? Please no spaces"
  read -r name
  FILE="src/main/resources/db/migration/V${DATE}__${name}.sql"
  touch "${FILE}"
  echo "Created ${FILE}"
}

case ${COMMAND} in
  new) new; ;;
  baseline) baseline ;;
  clean) clean ;;
  *)
    echo "Unknown command: ${COMMAND}"
    usage
    exit 1
esac



